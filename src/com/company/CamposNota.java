//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.company;

import java.util.ArrayList;

public class CamposNota {
    private String nName;
    private String nDireccion;
    private String nNumeroFactura;
    private String nIvaType;
    private String nDocumento;
    private String nFacturaType;
    private String monto;
    private String sucursal;
    private ArrayList<String[]> nItems;

    public CamposNota(String nName, String nDireccion, String nDocumento, String nIvaType, String nNumeroFactura, String nFacturaType, String nMonto, String sucursal) {
        this.nName = nName;
        this.nDireccion = nDireccion;
        this.nDocumento = nDocumento;
        this.nIvaType = nIvaType;
        this.nNumeroFactura = nNumeroFactura;
        this.nFacturaType = "NC" + nFacturaType;
        this.monto = nMonto;
        this.sucursal = sucursal;
    }

    public CamposNota() {
    }

    public String getnName() {
        return this.nName;
    }

    public void setnName(String nName) {
        this.nName = nName;
    }

    public String getnDocumento() {
        return this.nDocumento;
    }

    public void setnDocumento(String nDocumento) {
        this.nDocumento = nDocumento;
    }

    public String getnDireccion() {
        return this.nDireccion;
    }

    public void setnDireccion(String nDireccion) {
        this.nDireccion = nDireccion;
    }

    public String getnNumeroFactura() {
        return this.nNumeroFactura;
    }

    public void setnNumeroFactura(String nNumeroFactura) {
        this.nNumeroFactura = nNumeroFactura;
    }

    public ArrayList<String[]> getnItems() {
        return this.nItems;
    }

    public void setnItems(ArrayList<String[]> nItems) {
        this.nItems = nItems;
    }

    public String getnIvaType() {
        return this.nIvaType;
    }

    public void setnIvaType(String nIvaType) {
        this.nIvaType = nIvaType;
    }

    public String getnFacturaType() {
        return this.nFacturaType;
    }

    public void setnFacturaType(String nFacturaType) {
        this.nFacturaType = "NC" + nFacturaType;
    }

    public String getMonto() {
        return this.monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getSucursal() {
        return this.sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }
}
