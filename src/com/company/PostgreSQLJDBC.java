//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.company;

import java.awt.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;


public class PostgreSQLJDBC {
    private static Connection mConnection = null;
    private static final String SQL_POSTGRESQL_PROVIDER = "org.postgresql.Driver";
    private static final String SQL_CONNECTION_STRING = "jdbc:postgresql://";
    private static final String SQL_CONNECTION_PORT = ":5432/";
    private static final String SQL_CONNECTION_USER = "postgres";
    private static final String SQL_CONNECTION_PASS = "wkrdjqwnd";
    private static String serverName = "localhost";
    private static String mDBName = "lean";
    private static final PostgreSQLJDBC ourInstance = new PostgreSQLJDBC();

    static PostgreSQLJDBC getInstance() {
        return ourInstance;
    }

    private PostgreSQLJDBC() {
    }

    public static void setDBName(String stringDBName) {
        if (!stringDBName.isEmpty()) {
            mDBName = stringDBName;
        }

    }

    public static void setServer(String stringserverName) {
        if (!stringserverName.isEmpty()) {
            serverName = stringserverName;
        }

    }

    private static boolean abrirDataBase() {
        try {
            Class.forName("org.postgresql.Driver");
            mConnection = DriverManager.getConnection("jdbc:postgresql://" + serverName + ":5432/" + mDBName, "postgres", "wkrdjqwnd");
            System.out.println("Opened database successfully");
            return true;
        } catch (Exception var1) {
            var1.printStackTrace();
            System.err.println(var1.getClass().getName() + ": " + var1.getMessage());
            JOptionPane.showMessageDialog((Component)null, var1.getMessage(), "DB ERROR", 0);
            return false;
        }
    }

    private static boolean cerrarDataBase() {
        try {
            mConnection.close();
            System.out.println("DataBase Close successfully");
            return true;
        } catch (Exception var1) {
            var1.printStackTrace();
            System.err.println(var1.getClass().getName() + ": " + var1.getMessage());
            return false;
        }
    }

    public static ResultSet executeQueryDataBase(String mQuery) {
        try {
            abrirDataBase();
            Statement mConnectionStatement = mConnection.createStatement();
            ResultSet mResultSet = mConnectionStatement.executeQuery(mQuery);
            System.out.println("Query successfully");
            cerrarDataBase();
            return mResultSet;
        } catch (SQLException var3) {
            var3.printStackTrace();
            System.err.println(var3.getClass().getName() + ": " + var3.getMessage());
            return null;
        }
    }
}
