//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package impresion;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class PanelClientes extends JPanel {
    private static final long serialVersionUID = 1L;
    private JTextField textClientName;
    private JTextField textCuitNumber;
    private JTextField textDireccion;
    private String clienType;

    public PanelClientes() {
        this.setBorder(new TitledBorder((Border)null, "Datos del Cliente", 4, 2, (Font)null, Color.BLUE));
        this.setLayout(new GridLayout(3, 2, 5, 5));
        JLabel lblNewLabel = new JLabel("Nombre del Cliente");
        this.add(lblNewLabel);
        this.textClientName = new JTextField();
        this.add(this.textClientName);
        this.textClientName.setColumns(10);
        JLabel lblCuit = new JLabel("C.U.I.T");
        this.add(lblCuit);
        this.textCuitNumber = new JTextField();
        this.add(this.textCuitNumber);
        this.textCuitNumber.setColumns(10);
        JLabel lblDireccion = new JLabel("Direccion");
        this.add(lblDireccion);
        this.textDireccion = new JTextField();
        this.add(this.textDireccion);
        this.textDireccion.setColumns(10);
    }

    public String textClientNameString() {
        String mText = this.textClientName.getText();
        return mText.isEmpty() ? " " : mText;
    }

    public String textCuitNumberString() {
        String mText = this.textCuitNumber.getText();
        return mText.isEmpty() ? " " : mText;
    }

    public String textDireccionString() {
        String mText = this.textDireccion.getText();
        return mText.isEmpty() ? " " : mText;
    }

    public void setCampos(String[] arg0) {
        this.textClientName.setText(arg0[0]);
        this.textDireccion.setText(arg0[1]);
        this.textCuitNumber.setText(arg0[2]);
        this.clienType = arg0[3];
    }

    public String getClienType() {
        return this.clienType;
    }

    public void setClienType(String clienType) {
        this.clienType = clienType;
    }
}
