//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.company;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class PanelItems extends JPanel {
    private static final long serialVersionUID = 1L;
    private JTable table;
    private String[] tableTitle = new String[]{"Descripcion", "Cantidad", "Monto"};
    private String[][] dato = new String[][]{{"", null, null}};

    public PanelItems() {
        this.setLayout(new BoxLayout(this, 0));
        DefaultTableModel model = new DefaultTableModel(this.dato, this.tableTitle);
        this.table = new JTable(model);
        this.table.setSelectionMode(0);
        this.table.setSelectionBackground(Color.CYAN);
        this.table.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent arg0) {
                DefaultTableModel model;
                if (arg0.getKeyCode() == 10) {
                    model = (DefaultTableModel)PanelItems.this.table.getModel();
                    model.addRow(new Object[0]);
                } else if (arg0.getKeyCode() == 127) {
                    model = (DefaultTableModel)PanelItems.this.table.getModel();
                    model.removeRow(PanelItems.this.table.getSelectedRow());
                }

                PanelItems.this.getTableItems();
            }
        });
        JScrollPane jScroll = new JScrollPane(this.table);
        this.add(jScroll);
    }

    public ArrayList<String[]> getTableItems() {
        ArrayList<String[]> datos = new ArrayList();

        for(int x = 0; x < this.table.getRowCount(); ++x) {
            String[] row = new String[]{(String)this.table.getModel().getValueAt(x, 0), (String)this.table.getModel().getValueAt(x, 1), (String)this.table.getModel().getValueAt(x, 2)};
            datos.add(row);
        }

        System.out.println(datos.toString());
        return datos;
    }

    public void setTableItems(ArrayList<String[]> data) {
        DefaultTableModel model;
        if (data == null) {
            model = new DefaultTableModel(this.dato, this.tableTitle);
        } else {
            model = new DefaultTableModel((Object[][])data.toArray(new String[0][]), this.tableTitle);
        }

        this.table.setModel(model);
    }
}
