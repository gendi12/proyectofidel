//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.company;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TimerTask;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

public class Ventana extends JFrame implements PanelFactura.OnCompleteListener {
    private static final long serialVersionUID = 1L;
    private impresion.PanelClientes panelClientes;
    private PanelFactura panelFactura;
    private PanelItems panelItems;
    private static String nFactura;
    private static String tFactura;
    private JTextField textSerialPort;
    private JTextField textFieldServer;
    private CamposNota perfil;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Ventana frame = new Ventana();
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public Ventana() {
        DataController dc = DataController.getInstance();
        this.setResizable(false);
        this.getContentPane().setBackground(new Color(250, 240, 230));
        this.setBackground(SystemColor.scrollbar);
        this.setBounds(100, 100, 411, 331);
        this.setDefaultCloseOperation(3);
        this.getContentPane().setLayout((LayoutManager)null);
        this.panelClientes = new impresion.PanelClientes();
        this.panelClientes.setBounds(10, 11, 253, 104);
        this.getContentPane().add(this.panelClientes);
        this.panelItems = new PanelItems();
        this.panelItems.setBounds(10, 149, 389, 150);
        this.getContentPane().add(this.panelItems);
        this.panelFactura = new PanelFactura(this);
        this.panelFactura.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Factura N°", 4, 2, (Font)null, new Color(0, 0, 255)));
        this.panelFactura.setBounds(262, 11, 137, 76);
        this.getContentPane().add(this.panelFactura);
        JButton btnNewButton_1 = new JButton("Nota de Credito");
        btnNewButton_1.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                try {
                    ImprimirFiscal.setComPort(DataController.getCOMPORT().toUpperCase());
                    ImprimirFiscal.cargarCLiente(Ventana.this.perfil.getnName(), Ventana.this.perfil.getnDocumento(), Ventana.this.perfil.getnDireccion(), Ventana.this.perfil.getnFacturaType(), Ventana.this.perfil.getnIvaType());
                    ImprimirFiscal.cargarRemito(Ventana.this.perfil.getnNumeroFactura());
                    ImprimirFiscal.abrirRemito(Ventana.this.perfil.getnFacturaType());
                } catch (Exception var7) {
                    var7.printStackTrace();
                }

                Iterator var3 = Ventana.this.panelItems.getTableItems().iterator();

                while(var3.hasNext()) {
                    String[] object = (String[])var3.next();

                    try {
                        ImprimirFiscal.cargarItem(object[0], object[1], object[2], Ventana.this.panelFactura.getFacturaType());
                    } catch (Exception var6) {
                        var6.printStackTrace();
                    }
                }

                Double precio = ImprimirFiscal.getmontoTotal() * -1.0D;
                DecimalFormat df = new DecimalFormat("0.0000");
                Ventana.this.perfil.setMonto(df.format(precio).replace(",", "."));

                try {
                    DataController.register(ImprimirFiscal.cerrarRemito(), Ventana.this.perfil);
                } catch (Exception var5) {
                    var5.printStackTrace();
                }

            }
        });
        btnNewButton_1.setBounds(10, 115, 136, 23);
        this.getContentPane().add(btnNewButton_1);
        this.textSerialPort = new JTextField();
        this.textSerialPort.setEnabled(false);
        this.textSerialPort.setEditable(false);
        this.textSerialPort.setBounds(212, 126, 50, 20);
        this.getContentPane().add(this.textSerialPort);
        this.textSerialPort.setColumns(10);
        this.textFieldServer = new JTextField();
        this.textFieldServer.setEnabled(false);
        this.textFieldServer.setEditable(false);
        this.textFieldServer.setBounds(263, 126, 136, 20);
        this.getContentPane().add(this.textFieldServer);
        this.textFieldServer.setColumns(10);
        this.readConfFile();
        DataController.setDBName(this.textSerialPort.getText());
        DataController.setServer(this.textFieldServer.getText());
        DataController.createRow();
        this.process();
    }

    public void process() {
        System.out.println("Process->");

        try {
            Runtime.getRuntime().exec("taskkill /F /IM dbsynker.exe");
        } catch (Exception var2) {
            var2.printStackTrace();
        }

    }

    public TimerTask tarea1() {
        return new TimerTask() {
            public void run() {
                try {
                    DataController dc = DataController.getInstance();
                    String[] hola = DataController.getNCredito();
                    if (hola != null) {
                        Ventana.nFactura = hola[0];
                        Ventana.tFactura = hola[1];
                        ImprimirFiscal.setComPort(DataController.getCOMPORT().toUpperCase());
                        DataController.deleteNCredito(Ventana.nFactura);
                        Ventana.this.panelItems.setTableItems(DataController.getFDetalles(Ventana.nFactura, Ventana.tFactura));
                    }
                } catch (Exception var3) {
                    ;
                }

            }
        };
    }

    public void readConfFile() {
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader("config.txt"));
            StringBuilder sb = new StringBuilder();

            for(String line = br.readLine(); line != null; line = br.readLine()) {
                sb.append(line);
                sb.append(System.lineSeparator());
            }

            String everything = sb.toString();
            String[] hola = everything.split(";");
            if (hola.length != 0) {
                this.textFieldServer.setText(hola[0]);
                this.textSerialPort.setText(hola[1]);
            }
        } catch (IOException var14) {
            var14.printStackTrace();
            JOptionPane.showMessageDialog((Component)null, var14.getMessage(), "DB ERROR", 0);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException var13) {
                    var13.printStackTrace();
                }
            }

        }

    }

    public void OnComplete(String text) {
        this.panelClientes.setCampos(new String[]{"", "", "", ""});
        this.panelItems.setTableItems((ArrayList)null);
        String factura;
        if (text.length() <= 8) {
            factura = DataController.searchFacturaHeader() + "-" + "00000000".substring(text.length()) + text;
        } else {
            factura = text;
        }

        this.panelFactura.setFracturaString(factura);
        System.out.println("factura " + factura);
        String type = this.panelFactura.getFacturaType();
        String[] data = DataController.getFVentas(factura, type);
        if (data != null) {
            this.panelItems.setTableItems(DataController.getFDetalles(factura, type));
            String[] resultado;
            if (data[0].isEmpty()) {
                resultado = new String[]{"Cosumidor Final", " ", " ", "2"};
            } else {
                resultado = DataController.getClientes(data[0]);
            }

            this.panelClientes.setCampos(resultado);
            this.perfil = new CamposNota(resultado[0], resultado[1], resultado[2], resultado[3], factura, type, (String)null, data[1]);
            this.perfil = new CamposNota(resultado[0], resultado[1], resultado[2], resultado[3], factura, type, (String)null, data[1]);
        }
    }
}
