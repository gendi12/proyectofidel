//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.company;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DataController {
    private static final DataController ourInstance = new DataController();

    static DataController getInstance() {
        return ourInstance;
    }

    private DataController() {
        PostgreSQLJDBC.getInstance();
    }

    public static String[] getFVentas(String numeroFactura, String tipoFactura) {
        String selectFVenta = "SELECT dni,sucursal FROM fventas WHERE numfactura = '" + numeroFactura + "' AND tipofactura = '" + tipoFactura + "'";

        try {
            ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase(selectFVenta);
            mResultSet.next();
            return new String[]{mResultSet.getString(1), mResultSet.getString(2)};
        } catch (SQLException var4) {
            var4.printStackTrace();
            return null;
        }
    }

    public static String[] getClientes(String dni) {
        String selectClientes = "SELECT nombre,direccion,dni,resiva FROM clientes WHERE dni = '" + dni + "'";

        try {
            ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase(selectClientes);
            mResultSet.next();
            return new String[]{mResultSet.getString(1), mResultSet.getString(2), mResultSet.getString(3), mResultSet.getString(4)};
        } catch (SQLException var3) {
            var3.printStackTrace();
            return null;
        }
    }

    public static ArrayList<String[]> getFDetalles(String numeraFactura, String tipoFactura) {
        String selectFDetalle = "SELECT fitem,fcant,fpreuni FROM fdetalles WHERE numfactura = '" + numeraFactura + "' AND tipofactura = '" + tipoFactura + "'";
        ArrayList mData = new ArrayList();

        try {
            ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase(selectFDetalle);

            while(mResultSet.next()) {
                mData.add(new String[]{mResultSet.getString(1), mResultSet.getString(2), mResultSet.getString(3)});
            }

            return mData;
        } catch (SQLException var5) {
            var5.printStackTrace();
            return null;
        }
    }

    public static String[] getNCredito() {
        String var0 = "SELECT dni,casoesp FROM fistmpcab WHERE clientenombre = 'NCredito' LIMIT 1;";

        try {
            ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase("SELECT dni,casoesp FROM fistmpcab WHERE clientenombre = 'NCredito' LIMIT 1;");
            mResultSet.next();
            return new String[]{mResultSet.getString(1), mResultSet.getString(2)};
        } catch (SQLException var2) {
            return null;
        }
    }

    public static void deleteNCredito(String numeraFactura) {
        String deleteNCredito = "DELETE FROM fistmpcab WHERE dni = '" + numeraFactura + "';";

        try {
            PostgreSQLJDBC.executeQueryDataBase(deleteNCredito);
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    public static void setDBName(String data) {
        PostgreSQLJDBC.setDBName(data);
    }

    public static void setServer(String data) {
        PostgreSQLJDBC.setServer(data);
    }

    public static String getCOMPORT() {
        String var0 = "select valor2 from parametros where pname = 'fiscalimp'";

        try {
            ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase("select valor2 from parametros where pname = 'fiscalimp'");
            mResultSet.next();
            return "COM" + mResultSet.getString(1);
        } catch (SQLException var2) {
            var2.printStackTrace();
            return null;
        }
    }

    public static String createRow() {
        String var0 = "ALTER TABLE fventas ADD COLUMN ref_num character varying(100);";

        try {
            PostgreSQLJDBC.executeQueryDataBase("ALTER TABLE fventas ADD COLUMN ref_num character varying(100);");
        } catch (Exception var2) {
            var2.printStackTrace();
        }

        return null;
    }

    public static void register(String facturaNUM, CamposNota perfil) {
        String[] prefijo = perfil.getnNumeroFactura().split("-");
        facturaNUM = prefijo[0] + "-" + facturaNUM;
        String selectFVenta = "INSERT INTO fventas(hora, dni, clientenombre, monto, numfactura, tipofactura, sucursal, utime, fecha, borrado, ref_num)VALUES (current_time,'" + perfil.getnDocumento() + "','" + perfil.getnName() + "'," + perfil.getMonto() + ",'" + facturaNUM + "','" + perfil.getnFacturaType() + "'," + perfil.getSucursal() + ", current_timestamp, current_date,FALSE,'" + perfil.getnNumeroFactura() + "'); ";

        try {
            PostgreSQLJDBC.executeQueryDataBase(selectFVenta);
        } catch (Exception var5) {
            var5.printStackTrace();
        }

    }

    public static String searchFacturaHeader() {
        String var0 = "select valor3 from parametros where pname = 'fiscalimp'";

        try {
            ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase("select valor3 from parametros where pname = 'fiscalimp'");
            mResultSet.next();
            return mResultSet.getString(1);
        } catch (SQLException var2) {
            var2.printStackTrace();
            return null;
        }
    }
}
