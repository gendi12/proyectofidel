//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.company;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

public class PanelFactura extends JPanel {
    private static final long serialVersionUID = 1L;
    private JTextField textFactura;
    JRadioButton rdbtnNewRadioButton;
    JRadioButton rdbtnNewRadioButton_1;
    private PanelFactura.OnCompleteListener listenerFactura;

    public PanelFactura(PanelFactura.OnCompleteListener listener) {
        this.listenerFactura = listener;
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{43, 0};
        gridBagLayout.rowHeights = new int[2];
        gridBagLayout.columnWeights = new double[]{1.0D, 1.0D};
        gridBagLayout.rowWeights = new double[]{4.9E-324D, 1.0D};
        this.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "----", 4, 2, (Font)null, Color.BLUE));
        this.setLayout(gridBagLayout);
        ButtonGroup grupo = new ButtonGroup();
        this.textFactura = new JTextField();
        this.textFactura.setFocusTraversalKeysEnabled(false);
        this.textFactura.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == 10 || arg0.getKeyCode() == 9) {
                    PanelFactura.this.action1();
                    PanelFactura.this.listenerFactura.OnComplete(PanelFactura.this.textFacturaString());
                }

            }
        });
        GridBagConstraints gbc_textFactura = new GridBagConstraints();
        gbc_textFactura.fill = 2;
        gbc_textFactura.gridwidth = 2;
        gbc_textFactura.insets = new Insets(0, 0, 5, 5);
        gbc_textFactura.gridx = 0;
        gbc_textFactura.gridy = 0;
        gbc_textFactura.anchor = 11;
        this.add(this.textFactura, gbc_textFactura);
        this.textFactura.setColumns(10);
        this.rdbtnNewRadioButton = new JRadioButton("A");
        this.rdbtnNewRadioButton.addMouseListener(this.mouseClick());
        this.rdbtnNewRadioButton.setSelected(true);
        GridBagConstraints gbc_rdbtnNewRadioButton = new GridBagConstraints();
        gbc_rdbtnNewRadioButton.anchor = 17;
        gbc_rdbtnNewRadioButton.fill = 3;
        gbc_rdbtnNewRadioButton.insets = new Insets(0, 0, 0, 5);
        gbc_rdbtnNewRadioButton.gridx = 0;
        gbc_rdbtnNewRadioButton.gridy = 1;
        this.add(this.rdbtnNewRadioButton, gbc_rdbtnNewRadioButton);
        this.rdbtnNewRadioButton_1 = new JRadioButton("B");
        this.rdbtnNewRadioButton_1.addMouseListener(this.mouseClick());
        GridBagConstraints gbc_rdbtnNewRadioButton_1 = new GridBagConstraints();
        gbc_rdbtnNewRadioButton_1.anchor = 17;
        gbc_rdbtnNewRadioButton_1.gridx = 1;
        gbc_rdbtnNewRadioButton_1.gridy = 1;
        this.add(this.rdbtnNewRadioButton_1, gbc_rdbtnNewRadioButton_1);
        grupo.add(this.rdbtnNewRadioButton);
        grupo.add(this.rdbtnNewRadioButton_1);
    }

    private void action1() {
        String letters = this.textFactura.getText().replaceAll("[0-9^-]", "").toUpperCase();
        System.out.println("Letra " + letters);
        if (!letters.isEmpty() && letters != null) {
            if (letters.charAt(0) == 'A') {
                this.rdbtnNewRadioButton.setSelected(true);
            } else if (letters.charAt(0) == 'B') {
                this.rdbtnNewRadioButton_1.setSelected(true);
            }
        }

    }

    public void setFracturaString(String arg0) {
        this.textFactura.setText(arg0);
    }

    public String textFacturaString() {
        String text = this.textFactura.getText();
        return text.length() <= 8 ? text.replaceAll("[^\\d.]", "") : text.replaceAll("[^\\d-]", "");
    }

    public String getFacturaType() {
        if (this.rdbtnNewRadioButton.isSelected()) {
            return "A";
        } else {
            return this.rdbtnNewRadioButton_1.isSelected() ? "B" : null;
        }
    }

    public MouseAdapter mouseClick() {
        return new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                PanelFactura.this.listenerFactura.OnComplete(PanelFactura.this.textFacturaString());
            }
        };
    }

    public interface OnCompleteListener {
        void OnComplete(String var1);
    }
}
